using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] ObjectPlace _objectPlace;
    public Slider boxLimit;

    //BoxLimit is equal to the blockAmount
    private void Update()
    {
        boxLimit.value = _objectPlace.blockAmount;
    }
}
