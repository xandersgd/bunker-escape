using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LargeButton : MonoBehaviour
{
    public bool doorActive;

    //The door object
    public GameObject Door;

    //If a block with the tag "PickUp" is ontop of the button Set doorActive to true 
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PickUp")
        {
            doorActive = true;
        }
    }

    //If doorActive is true turn the door off otherwise the door is on
    private void Update()
    {
        if (doorActive == true)
        {
            Door.SetActive(false);
        }
        else
        {
            Door.SetActive(true);
        }
    }
}
