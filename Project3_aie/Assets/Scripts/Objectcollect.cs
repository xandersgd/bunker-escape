using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectCollect : MonoBehaviour
{

    public GameObject gun;
    public GameObject text;

    //If the player presses F within the trigger, deletes the object this is attached to, destroys text and activates the gun.
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && Input.GetKey(KeyCode.F))
        {
            Destroy(this.gameObject);
            Destroy(text);
            gun.gameObject.SetActive(true);
        }
    }
}
