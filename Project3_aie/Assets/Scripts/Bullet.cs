using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private void Start()
    {
        Destroy(this.gameObject, 2f);
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Metalic")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        else if (other.gameObject.tag == "Organic")
        {
            Destroy(gameObject);
        }               
    }
}
