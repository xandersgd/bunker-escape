using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPickup : MonoBehaviour
{
    public GameObject pickUp;
    private Rigidbody _rigidbody;
    public GameObject prefab;
    public LargeButton _blockButton;


    private void Update()
    {

        RaycastHit hit;

        //Raycast distance is 3
        float distanceOfRay = 3;
        
        //If the raycast hits something within its tange and the E key is pressed and the object that is hit has a tag of "PickUp"
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, distanceOfRay) && Input.GetKey(KeyCode.E) 
            && hit.transform.gameObject.tag == "PickUp")
        {
            //Get the BoxCollider of the hit object and set it to a trigger
            Collider _col = gameObject.GetComponent<BoxCollider>();
            _col.isTrigger = true;

            //Get the rigidbody and destroy it
            _rigidbody = gameObject.GetComponent<Rigidbody>();
            Destroy(_rigidbody);

            //The hit targets position and rotation are set to the gameObject "pickUp"
            hit.transform.position = pickUp.transform.position;
            hit.transform.rotation = pickUp.transform.rotation;

            //The hit target is parented under the gameObject "pickUp"
            hit.transform.parent = pickUp.gameObject.transform.parent;

            //Set the doorActive bool to false, so the door isnt permanently open after picking up the cube
            _blockButton.doorActive = false;
        }

        //If the raycast hits the object within its range and the E key is released
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit) && Input.GetKeyUp(KeyCode.E))
        {
            //Get the BoxCollider and set it to not a trigger
            Collider _col = gameObject.GetComponent<BoxCollider>();
            _col.isTrigger = false;

            //Give the hit target a game object 
            gameObject.AddComponent<Rigidbody>();
            _rigidbody = gameObject.GetComponent<Rigidbody>();

            //Give the rigidbody to the prefab gameObject
            prefab.AddComponent<Rigidbody>();
            _rigidbody = prefab.GetComponent<Rigidbody>();

            //Turn both the hit target and the prefab targets to no parent
            transform.parent = null;
            prefab.transform.parent = null;
        }


        //If the raycast hits something within its tange and the E key is pressed and the object that is hit has a tag of "BoxCreate"
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, distanceOfRay) && Input.GetKey(KeyCode.E) 
            && hit.transform.gameObject.tag == "BoxCreate")
        {
            //Get the BoxCollider of the prefab object and set it to a trigger
            Collider _col = gameObject.GetComponent<BoxCollider>();
            _col.isTrigger = false;

            //Get the rigidbody and destroy it
            _rigidbody = prefab.GetComponent<Rigidbody>();
            Destroy(_rigidbody);

            //The prefab/hit targets position and rotation are set to the gameObject "pickUp"
            hit.transform.position = pickUp.transform.position;
            hit.transform.rotation = pickUp.transform.rotation;

            //Put the hit prefab under the parent of pickUp gameObject
            hit.transform.parent = pickUp.gameObject.transform.parent;
        }
        //Grab the rotation of the camera and the rotation and times that by the distance of the raycast
        Debug.DrawLine(Camera.main.transform.position, Camera.main.transform.forward * distanceOfRay);
    }
}
