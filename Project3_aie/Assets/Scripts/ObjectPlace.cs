using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlace : MonoBehaviour
{
    public GameObject Barrel;
    public GameObject blockPrefab;
    public Transform blockSpawnpoint;
    public float blockSpeed;
    public int blockAmount = 0;
    public ParticleSystem blockPlace1;
    public ParticleSystem blockPlace2;
    public ParticleSystem blockPlace3;
    public ParticleSystem blockPlace4;

    public ParticleSystem Place1;
    public ParticleSystem Place2;
    public ParticleSystem Place3;
    public ParticleSystem Place4;


    private void Update()
    {
        //If the rightClick is pressed and blockAmount is less than 10
        if (Input.GetKeyDown(KeyCode.Mouse1) && blockAmount < 10)
        {
            //Do the ShootBlock function and add 1 to the blockAmount
            ShootBlock();
            blockAmount++;
            blockPlace1.Play();
            blockPlace2.Play();
            blockPlace3.Play();
            blockPlace4.Play();
            Place1.Play();
            Place2.Play();
            Place3.Play();
            Place4.Play();
        }
    }

    void ShootBlock()
    {
        //Spawn the blockPrefab at the blackSpawnpoint position and rotation
        GameObject GO = Instantiate(blockPrefab, blockSpawnpoint.position, Quaternion.identity) as GameObject;

        //Get the Rigidbody and apply force determined by the blockSpeed and barrels direction
        GO.GetComponent<Rigidbody>().AddForce(Barrel.transform.forward * blockSpeed, ForceMode.Impulse);
    }
}