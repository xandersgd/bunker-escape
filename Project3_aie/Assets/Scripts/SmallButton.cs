using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallButton : MonoBehaviour
{
    public GameObject Door;
    public GameObject text;
    public GameObject text2;
    private void OnTriggerStay(Collider other)
    {
        //If a playerenters the trigger and presses the F key
        if (other.gameObject.tag == "Player" && Input.GetKey(KeyCode.F))
        {
            //Destroy Door, and both text gameObjects
            Destroy(Door);
            Destroy(text);
            Destroy(text2);
        }
    }
}
