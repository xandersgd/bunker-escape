using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
public class ObjectDelete : MonoBehaviour
{
    [SerializeField] ObjectPlace _objectPlace;
    public ParticleSystem objectDelete;
    public Animator gunshot;
    bool isPlaying;
    bool canShoot;

    private void Start()
    {
        canShoot = true;
    }

    void Update()
    {
        RaycastHit hit;

        if (isPlaying == false)
        {
            gunshot.SetBool("Shot", false);
        }

        //The distance of the raycast is 100
        float distanceOfRay = 100;


        //If the raycast hits an object with the tag "Metalic" within its range and it gets the leftclick down, Delete the object
        if (canShoot == true && Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, distanceOfRay) && Input.GetKeyDown(KeyCode.Mouse0) 
            && hit.transform.gameObject.tag == "Metalic")
        {
            Destroy(hit.transform.gameObject);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && canShoot == true) 
        {
            gunshot.SetBool("Shot", true);
            StartCoroutine(Timer());
            objectDelete.Play();
        }

        //If the raycast hits an object with the tag "BoxCreate" within its range and it gets the leftclick down, Delete the object
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, distanceOfRay) && Input.GetKeyDown(KeyCode.Mouse0) 
            && hit.transform.gameObject.tag == "BoxCreate")
        {
            Destroy(hit.transform.gameObject);
            //If the BoxCreate box is deleted minus one from blockAmount
            _objectPlace.blockAmount--;
        }
                //Grab the rotation of the camera and the rotation and times that by the distance of the raycast
                Debug.DrawLine(Camera.main.transform.position, Camera.main.transform.forward * distanceOfRay);
        StartCoroutine(Timer());
    }
        IEnumerator Timer()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                gunshot.SetBool("Shot", true);
                objectDelete.Play();
                isPlaying = true;
                canShoot = false;
                yield return new WaitForSeconds(0.3f);
                isPlaying = false;
                canShoot = true;
            }
        }
}
