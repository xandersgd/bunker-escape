using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public GameObject teleportNode;
    public Transform boxSpawn;

    [SerializeField] CharacterController _cc;
    [SerializeField] ObjectPlace _objectPlace;

    
    private void OnTriggerEnter(Collider other)
    {
        //If the Player enters the trigger
        if (other.tag == "Player")
        {
            //Disable the CharacterController
            _cc.enabled = false;

            //Transform the position and rotation to the teleportNode gameObjects position and rotation
            other.transform.position = teleportNode.transform.position;
            other.transform.rotation = teleportNode.transform.rotation;

            //Re-enable the CharacterController
            _cc.enabled = true;
        }

        //If an object with "BoxCreate" enters the trigger
        if (other.tag == "BoxCreate")
        {
            //Destroy the "BoxCreate" object
            Destroy(other.gameObject);

            //Subtract 1 from blockAmount
            _objectPlace.blockAmount--;
        }

        if (other.tag == "PickUp")
        {
            other.gameObject.transform.position = boxSpawn.position; 
        }
    }

    private void OnDrawGizmos()
    {
        //Draw a blue line between the respawn box collider and the teleportNode
        if (teleportNode)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, teleportNode.transform.position);
        }
    }
}
