using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private Transform camTransform;
    private CharacterController cc;

    //Speed settings for walking, running, and gravity
    public float walkSpeed = 5;
    public float runSpeed = 8;
    public float gravity = -9.85f;

    //Camera angle, and max and minimum possible camera angle
    public float viewAngle;
    public float minViewAngle, maxViewAngle;

    //On start lock the cursor and turn the cursor visibility off
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    //camTransform is Camera.main.transform and cc is the CharacterController
    private void Awake()
    {
        camTransform = Camera.main.transform;
        cc = GetComponent<CharacterController>();
    }

    
    void Update()
    {
        //Get the vertical and horizontal movement inputs and transform them forwards and horizontally
        Vector3 desiredDirection = camTransform.forward * Input.GetAxis("Vertical") + camTransform.right * Input.GetAxis("Horizontal");
        desiredDirection.y = 0;
        desiredDirection.Normalize();

        //if the input Run is active change the walk speed to run speed
        desiredDirection *= Input.GetButton("Run") ? runSpeed : walkSpeed;

        //Y is equal to the gravity float
        desiredDirection.y = gravity;

        cc.Move(desiredDirection * Time.deltaTime);

        //Getting the mouse Y input to move the camera on the Y axis
        viewAngle -= Input.GetAxis("Mouse Y");

        //Set the max and minimum viewing angle
        viewAngle = Mathf.Clamp(viewAngle, minViewAngle, maxViewAngle);

        transform.Rotate(0, Input.GetAxis("Mouse X"), 0);
        camTransform.rotation = Quaternion.Euler(viewAngle, transform.eulerAngles.y, 0);
    }
}
